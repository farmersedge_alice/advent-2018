#!/usr/bin/env python3
from collections import defaultdict
from dataclasses import dataclass
import re, sys
import string

data=list(list(open("input.txt"))[0].strip())

def react(data):
    previousLength=0
    start=0
    while len(data)!=previousLength:
        previousLength=len(data)
        for i in range(max(start-10, 0), len(data)-1): # start-10 is just a way overkill backtrack Just To Be Safe. it could probably be start-2
            a,b=data[i:i+2]
            if (a.lower()==b.lower()) and (a.islower()!=b.islower()):
                start=i #save a lot of backtracking
                del(data[i:i+2])
                break
    return len(data)

def reactWithout(data, char):
    return react([c for c in data if c.lower() != char])

results={c: reactWithout(data, c) for c in string.ascii_lowercase}
print(sorted(results.items(), key=lambda i: i[1])[0][1])
