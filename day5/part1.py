#!/usr/bin/env python3
from collections import defaultdict
from dataclasses import dataclass
import re, sys

data=list(list(open("input.txt"))[0].strip())

previousLength=0

start=0

while len(data)!=previousLength:
    previousLength=len(data)
    for i in range(max(start-10, 0), len(data)-1):
        a,b=data[i:i+2]
        if (a.lower()==b.lower()) and (a.islower()!=b.islower()):
            start=i #save a lot of backtracking
            del(data[i:i+2])
            break
print(len(data))
