#!/usr/bin/env python3
from collections import defaultdict
from dataclasses import dataclass
import re, sys

lines = list(open("input.txt"))

playerCount, lastVal=[int(i) for i in re.findall(r'(\d+)', open("input.txt").read().strip())]

elves=[[] for i in range(playerCount)]

currentElf=1
currentMarble=0

# clockwise
def cw(start, num):
    return (start+num) % len(circle)

circle=[0,2,1]
current=1

def insertMarble(marble, rightOf):
    if rightOf==0:
        circle.append(marble)
        return len(circle)-1
    else:
        circle.insert(rightOf, marble)
        return rightOf

for marble in range(3, lastVal):
    currentElf+=1
    currentElf%=playerCount

    if marble % 23 > 0:
        current=insertMarble(marble, cw(current, 2))
    else:
        elves[currentElf].append(
                marble
                )
        popIndex=cw(current, -7)
        elves[currentElf].append(
                circle.pop(popIndex)
                )
        current=popIndex%len(circle)
    #print("["+str(currentElf+1)+"]", " ".join(("("+str(circle[i])+")" if i==current else str(circle[i])) for i in range(len(circle))))

print(max(sum(elf) for elf in elves))
