#!/usr/bin/env python3
import re

playerCount, lastVal=[int(i) for i in re.findall(r'(\d+)', open("input.txt").read().strip())]

class Marble:
    def __init__(self, value):
        self.left=self
        self.right=self
        self.value=value

    def remove(self):
        self.left.right=self.right
        self.right.left=self.left
        return self

    def insert(self, left, right):
        self.left=left
        self.right=right
        left.right=self
        right.left=self
        return self

    def getLeft(self, n=1):
        if n==0:
            return self
        elif n==1:
            return self.left
        else:
            return self.left.getLeft(n-1)

    def getRight(self, n=1):
        if n==0:
            return self
        elif n==0:
            return self.right
        else:
            return self.right.getRight(n-1)

elves=[0]*playerCount
currentElf=0
current=Marble(0)
    
for marble in (Marble(i) for i in range(0, lastVal*100)):
    currentElf+=1
    currentElf%=playerCount

    if marble.value % 23 > 0:
        current=marble.insert(current.getRight(1), current.getRight(2))
    else:
        elves[currentElf]+=marble.value
        elves[currentElf]+=current.getLeft(7).remove().value
        current=current.getLeft(6)

print(max(elves))
