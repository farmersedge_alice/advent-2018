#!/usr/bin/env python3
from collections import defaultdict
from dataclasses import dataclass
import re, sys

@dataclass
class Point:
    x: int
    y: int
    vx: int
    vy: int

    def move(self, time):
        self.x+=self.vx*time
        self.y+=self.vy*time
        return self

    def predict(self, time):
        return Point(
                self.x+self.vx*time,
                self.y+self.vy*time,
                self.vx,
                self.vy
                )

points=[Point(*[int(i) for i in re.findall(r'([0-9-]+)', line)]) for line in open("input.txt")]
sizes=[]

def getSize(points):
    top=[None, None]
    bot=[None, None]
    for p in points:
        if top[0] is None or p.x > top[0]:
            top[0]=p.x             
        if top[1] is None or p.y > top[1]:
            top[1]=p.y             
        if bot[0] is None or p.x < bot[0]:
            bot[0]=p.x             
        if bot[1] is None or p.y < bot[1]:
            bot[1]=p.y
    return (
            (top[0]-bot[0])+1,
            (top[1]-bot[1])+1
            )

def getOffset(points):
    return (
            min(points, key=lambda i: i.x).x,
            min(points, key=lambda i: i.y).y
            )

def getConvergedTime(points):
    t=0
    while True:
        sizes.append(getSize(point.predict(t) for point in points))
        if len(sizes) > 1 and sizes[-2][0] < sizes[-1][0]:
            return t-1
        else:
            t+=1

t=getConvergedTime(points)
for point in points:
    point.move(t)
size=getSize(points)

offset=getOffset(points)
for point in points:
    point.x-=offset[0]
    point.y-=offset[1]

image=[[False]*size[0] for i in range(size[1])]

for point in points:
    image[point.y][point.x]=True

for row in image:
    print(''.join('#' if col else '.' for col in row))
