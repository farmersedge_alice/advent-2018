#!/usr/bin/env python3
from collections import defaultdict

twice=0
thrice=0
with open("input.txt") as f:
    for line in f:
        chars=defaultdict(lambda: 0)
        for char in line:
            chars[char]+=1
        reps=set(chars.values())
        if(reps.intersection({2})):
            twice+=1
        if(reps.intersection({3})):
            thrice+=1

print(twice*thrice)
