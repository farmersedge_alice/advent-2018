#!/usr/bin/env python3
from collections import defaultdict
import sys

ids=[]
with open("input.txt") as f:
    for line in f:
        for i in ids:
            diff=0
            result=""
            for c1, c2 in zip(line, i):
                if c1 != c2:
                    diff+=1
                    if diff>1: break
                else:
                    result+=c1
            if diff==1:
                print(result)
                sys.exit(0)
        ids.append(line)
