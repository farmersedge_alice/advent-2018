#!/usr/bin/env python3
from collections import defaultdict
import re

#guards are an array of minutes
guards=defaultdict(lambda: defaultdict(lambda: 0))

guard=None
sleep=0
for line in sorted(list(open("input.txt"))):
    m=re.search('Guard #(\d+) begins shift', line)
    if m:
        guard=guards[m.group(1)]
    m=re.search('(\d+)\] falls', line)
    if m:
        sleep=int(m.group(1))
    m=re.search('(\d+)\] wakes', line)
    if m:
        wake=int(m.group(1))
        for i in range(sleep, wake):
            guard[i]+=1

sleepiestGuard=sorted(guards.items(), key=lambda i: sum(i[1].values()), reverse=True)[0][0]
sleepiestMinute=sorted(guards[sleepiestGuard].items(), key=lambda i: i[1], reverse=True)[0][0]
print(int(sleepiestGuard)*sleepiestMinute)
