#!/usr/bin/env python3
import re
from dataclasses import dataclass

cloth=[ [ 0 for i in range(1000) ] for i in range(1000) ]

@dataclass
class claim:
    claimId: int
    x: int
    y: int
    w: int
    h: int

    def __str__(self):
        return str(self.claimId)

with open("input.txt") as f:
    claims=[claim(*[int(v) for v in re.match('#(\d+) @ (\d+),(\d+): (\d+)x(\d+)', line).groups()]) for line in f]

for claim in claims:
    for r in range(claim.x, claim.w + claim.x):
        for c in range(claim.y, claim.h+claim.y):
            cloth[r][c] += 1

def checkClaim(cloth, claim):
    for r in range(claim.x, claim.w + claim.x):
        for c in range(claim.y, claim.h+claim.y):
            if cloth[r][c] > 1:
                return False
    return True

for claim in claims:
    if checkClaim(cloth, claim):
        print(claim)
