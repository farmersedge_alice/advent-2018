#!/usr/bin/env python3
import re

cloth=[ [ 0 for i in range(1000) ] for i in range(1000) ]

with open("input.txt") as f:
    for line in f:
        i, x, y, w, h = [int(g) for g in re.match('#(\d+) @ (\d+),(\d+): (\d+)x(\d+)', line).groups()]
        for r in range(x, w+x):
            for c in range(y, h+y):
                cloth[r][c] += 1

print(sum([sum([ 1 if v>1 else 0 for v in row ]) for row in cloth]))
