#!/usr/bin/env python3
from collections import defaultdict
from dataclasses import dataclass
from functools import reduce
import re, sys

def distance(a, b):
    return abs(a[0]-b[0]) + abs(a[1]-b[1])

def distanceSum(point, points):
    return sum([distance(point, p) for p in points])

lines = list(open("input.txt"))

points=[[int(i) for i in line.strip().split(", ")] for line in lines if line.strip() != ""]

offset=[
        min(points, key=lambda i: i[0])[0],
        min(points, key=lambda i: i[1])[1]
        ]

for c in points:
    c[0]-=offset[0]
    c[1]-=offset[1]

maximum=[
        max(points, key=lambda i: i[0])[0],
        max(points, key=lambda i: i[1])[1]
        ]

safePoints=0
for x in range(0, maximum[0]):
    for y in range(0, maximum[1]):
        if distanceSum([x,y], points) < 10000:
            safePoints+=1

print(safePoints)
