#!/usr/bin/env python3
from collections import defaultdict
from dataclasses import dataclass
from functools import reduce
import re, sys

def distance(a, b):
    return abs(a[0]-b[0]) + abs(a[1]-b[1])
lines = list(open("input.txt"))

points=[[int(i) for i in line.strip().split(", ")] for line in lines if line.strip() != ""]

offset=[
        min(points, key=lambda i: i[0])[0],
        min(points, key=lambda i: i[1])[1]
        ]

for c in points:
    c[0]-=offset[0]
    c[1]-=offset[1]

maximum=[
        max(points, key=lambda i: i[0])[0],
        max(points, key=lambda i: i[1])[1]
        ]

space=[
        [-1]*(maximum[1])
        for i in range(0,maximum[0])
        ]

for x in range(0, maximum[0]):
    for y in range(0, maximum[1]):
        distances=sorted({pi: distance((x,y), points[pi]) for pi in range(0, len(points))}.items(), key=lambda i: i[1])
        if distances[0][1] == distances[1][1]:
            space[x][y] = -1
        else:
            space[x][y] = distances[0][0]

infPoints=set()
for p in space[0] + \
        space[maximum[0]-1] + \
        [space[i][0] for i in range(0, maximum[0])] + \
        [space[i][maximum[0]-1] for i in  range(0, maximum[0])]:
    infPoints.add(p)

for i in space:
    print("".join([(('8' if p in infPoints else '*') if p!=-1 else '.') for p in i]))

pointCount=defaultdict(int)
for x in range(0, maximum[0]):
    for y in range(0, maximum[1]):
        if space[x][y] not in infPoints:
            pointCount[space[x][y]]+=1

print(sorted(pointCount.items(), key=lambda i: i[1], reverse=True)[0][1])
