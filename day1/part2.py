#!/usr/bin/env python3

def getFirstRepeatingFrequency(filename):
    frequencies=set()
    frequency=0
    while True:
        with open(filename) as f:
            for line in f:
                frequency+=int(line)
                if frequencies.intersection({frequency}):
                    return frequency
                else:
                    frequencies.add(frequency)

print(getFirstRepeatingFrequency("input.txt"))
