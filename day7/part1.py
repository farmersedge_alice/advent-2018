#!/usr/bin/env python3
from collections import defaultdict
from dataclasses import dataclass
import re, sys

deps=[]
steps=set()

deps=[re.findall(r'tep ([A-Z])', l) for l in open("input.txt")]
for dep in deps:
    steps.update(dep)

string=""
while steps:
    notHeads=set()
    for dep in deps:
        notHeads.add(dep[1])
    heads=sorted(steps.difference(notHeads))
    head=heads[0]
    string+=head
    steps.remove(head)
    for i in reversed(range(len(deps))):
        if deps[i][0] == head:
            del(deps[i])
print(string)
