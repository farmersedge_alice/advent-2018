#!/usr/bin/env python3
from collections import defaultdict
from dataclasses import dataclass
import re, sys, string

deps=[]
steps={}

deps=[re.findall(r'tep ([A-Z])', l) for l in open("input.txt")]
s=set()
for dep in deps:
    s.update(dep)
steps={step: 61+string.ascii_uppercase.index(step) for step in s}
print(steps)

s=""
time=0
currentTasks=set()
while steps:
    notHeads=set()
    for dep in deps:
        notHeads.add(dep[1])
    heads=sorted(set(steps.keys()).difference(notHeads))

    h=0
    while len(currentTasks)<5 and len(heads) > h:
        currentTasks.add(heads[h])
        h+=1

    for task in currentTasks:
        steps[task]-=1
    print(sorted(list(currentTasks)))
    for head in [h for h in heads if steps[h]==0]:
        if head in currentTasks:
            currentTasks.remove(head)
        s+="".join(sorted(head))
        del(steps[head])
        for i in reversed(range(len(deps))):
            if deps[i][0] in head:
                del(deps[i])
    time+=1
print(s)
print(time)
