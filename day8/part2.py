#!/usr/bin/env python3
from collections import defaultdict
from dataclasses import dataclass
import re, sys

@dataclass
class Node:
    childs: list
    metas: list
    def value(self):
        if len(self.childs) == 0:
            return sum(self.metas)
        else:
            return sum([
                self.childs[meta-1].value()
                for meta in self.metas
                if meta-1 < len(self.childs)
                ])

data = list(map(int, open("input.txt").read().strip().split(" ")))

metaSum=0

def readNode():
    global metaSum
    numChilds=data.pop(0)
    numMetas=data.pop(0)
    childs=[readNode() for i in range(numChilds)]
    metas=[data.pop(0) for i in range(numMetas)]
    metaSum+=sum(metas)
    return Node(childs=childs, metas=metas)

root=readNode()
print(metaSum)
print(root.value())
