my [advent of code 2018](https://adventofcode.com/2018/) solutions. each day
challenge expects `input.txt` in its respective directory.

## requirements

- bash
- python3.7
